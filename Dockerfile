FROM python:3.8.0

EXPOSE 8000

WORKDIR /Code/nikita/LEARNING/QuestionProject
COPY Pipfile* /Code/nikita/LEARNING/QuestionProject/
RUN pip install pipenv
RUN pipenv install

COPY . .

CMD [ "pipenv", "run", "python", "manage.py runserver" ]